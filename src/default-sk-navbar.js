
import { SkNavbarImpl }  from '../../sk-navbar/src/impl/sk-navbar-impl.js';

export class DefaultSkNavbar extends SkNavbarImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'navbar';
    }

    get bodyEl() {
        if (! this._bodyEl) {
            this._bodyEl = this.comp.el.querySelector('.sk-navbar');
        }
        return this._bodyEl;
    }

    open() {
        this.bodyEl.classList.add('sk-navbar-open');
    }

    close() {
        this.bodyEl.classList.remove('sk-navbar-open');
    }

    afterRendered() {
        super.afterRendered();
        this.bodyEl.innerHTML = this.contentsState || this.comp.contentsState;
        this.setAlignStyling();
        if (this.comp.hasAttribute('panel')) {
            this.bodyEl.classList.add('has-panel');
        }
        this.renderPanelTpl();
        if (this.comp.getAttribute('sticky') !== 'false') {
            this.bindSticky(this.panelEl, 'panel');
            this.bindSticky(this.bodyEl, 'body');
        }
    }
}
